extends Node2D

signal rects_moved

const RED = Color("#E9967A")
const GREEN = Color("#8FBC8F")
const YELLOW = Color("#FFE4B5")
const BLUE = Color("#5F9EA0")
const PURPLE = Color("#7B68EE")
const PINK = Color("#FFB6C1")

const MAX_WIDTH = 1920
const MAX_HEIGHT = 1080
const HALF_WIDTH = floor(MAX_WIDTH / 2)
const HALF_HEIGHT = floor(MAX_HEIGHT / 2)

export var SIZE_DIVISOR = 50

export var rects_to_draw = 50
export var rect_min_horiz = 40
export var rect_max_horiz = 50
export var rect_min_vert = 40
export var rect_max_vert = 50
export var move_mult = 3.5


var base_colors = [RED,GREEN,BLUE,YELLOW,PURPLE,PINK]
var started = true
var generated = true

var rng = RandomNumberGenerator.new()
#var font_path = "res://Custom Fonts/arcadepi.ttf"

var colors = []
var buttons = []
var labels = []
var rects = []
var sliders = {}

var static_label_node = Node2D.new()
var dynamic_label_node = Node2D.new()
var button_node = Node2D.new()

var time_since_move = 0

export (PackedScene) var slider_scene = preload("res://ControlHSlider.tscn")

var button_data = [
    'generate,pressed,generate_rects,' + str(MAX_WIDTH - 250) + ',50',
    'reset,pressed,_reset,' + str(MAX_WIDTH - 150) + ',' + str(MAX_HEIGHT - 50),
]
var label_data = [
    'rects_to_draw,rects',
    'rect_min_horiz,min width',
    'rect_max_horiz,max width',
    'rect_min_vert,min height',
    'rect_max_vert,max height',
    'move_mult,speed',
]
var slider_data = [
    # slider_name,min_value,max_value,step
    'rects_to_draw,1,200',
    'rect_min_horiz,10,199',
    'rect_max_horiz,10,200',
    'rect_min_vert,10,199',
    'rect_max_vert,10,200',
    'move_mult,1,10,0.1',
]

var count_labels = {}


# Called when the node enters the scene tree for the first time.
func _ready():
    # set background color
    VisualServer.set_default_clear_color(Color(0.1,0.1,0.1,1.0))

    static_label_node.name = 'static_labels'
    dynamic_label_node.name = 'dynamic_labels'
    button_node.name = 'buttons'
    $UserInterface.add_child(static_label_node)
    $UserInterface.add_child(dynamic_label_node)
    $UserInterface.add_child(button_node)

    # randomize the randomizer
    rng.randomize()

    # generate objects
    generate_buttons()
    generate_labels()

    generate_sliders()


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
    if Input.is_action_just_released("generate"):
        generate_rects()

    update_lever_values()

    if not rects:
        return

    time_since_move += delta

    var moved = false

    for i in range(rects.size()):
        for i2 in range(rects.size()):
            if rects[i].intersects(rects[i2]) and rects[i] != rects[i2]:
                var size_x = rects[i].size.x
                var size_y = rects[i].size.y
                var slider_mult = sliders['move_mult'].value
                var move_multiplier = rng.randi_range(max(slider_mult - 2, 0), slider_mult + 0.1)
                var x_move = size_x / SIZE_DIVISOR * move_multiplier
                var y_move = size_y / SIZE_DIVISOR * move_multiplier
                moved = true

                if rects[i].position.x < rects[i2].position.x:
                    # if to left, move left
                    rects[i].position.x  -= x_move
                    rects[i2].position.x += x_move
                    set_calculating_state()
                else:
                    # if to right, move right
                    rects[i].position.x  += x_move
                    rects[i2].position.x -= x_move
                    set_calculating_state()

                if rects[i].position.y < rects[i2].position.y:
                    # if above, move up
                    rects[i].position.y  -= y_move
                    rects[i2].position.y += y_move
                    set_calculating_state()
                else:
                    # else move down
                    rects[i].position.y  += y_move
                    rects[i2].position.y -= y_move
                    set_calculating_state()

    if moved:
        $UserInterface/Elapsed/ElapsedLabel.total_time += delta
        emit_signal("rects_moved")

    if time_since_move > 0.5 and rects:
        $UserInterface/Calculating.hide()

    update()


func set_calculating_state():
    time_since_move = 0
    $UserInterface/Calculating.show()


func _draw():
    for i in range(rects.size()):
        draw_rect(rects[i], colors[i])


func generate_buttons():
    for i in range(button_data.size()):
        var button_details = button_data[i].split(',')
        var button = Button.new()
        button.text = button_details[0]
        button.connect(button_details[1], self, button_details[2])
        button.rect_size.x = 100
        var x = 1
        # give pairs a gap using modulus
        var y = 85 * i + 20 * (i % 2) - 20
        if len(button_details) > 3:
            x = float(button_details[3]) - button.rect_size.x / 2
        if len(button_details) > 4:
            y = float(button_details[4]) - button.rect_size.y / 2
        button.set_position(Vector2(x, y))
        button_node.add_child(button)


func generate_labels():
    generate_static_labels()
    generate_dynamic_labels()


func generate_static_labels():
    for i in range(label_data.size()):
        var label_details = label_data[i].split(',')
        var label = RichTextLabel.new()
        label.rect_size = Vector2(350, 100)
        label.set_position(Vector2(MAX_WIDTH - 350, 145 + 135 * i))
        label.text = label_details[1]
        labels.append(label)
        static_label_node.add_child(label)


func generate_dynamic_labels():
    for i in range(label_data.size()):
        var label_details = label_data[i].split(',')
        var count_label = RichTextLabel.new()
        count_label.rect_size = Vector2(250, 100)
        count_label.set_position(Vector2(MAX_WIDTH - 350, 210 + 135 * i))
        # use get to read var contents from string key of input data
        count_label.text = str(get(label_details[0]))
        count_labels[label_details[0]] = count_label
        dynamic_label_node.add_child(count_label)


func generate_sliders():
    for i in range(slider_data.size()):
        var slider_details = slider_data[i].split(',')
        var slider = slider_scene.instance()

        var slider_name = slider_details[0]
        var min_value = 0
        var max_value = 1
        var step = 1
        if len(slider_details) > 1:
            min_value = int(slider_details[1])
        if len(slider_details) > 2:
            max_value = int(slider_details[2])
        if len(slider_details) > 3:
            step = float(slider_details[3])

        slider.initialize(
            slider_name,
            Vector2(200, 80),
            Vector2(MAX_WIDTH - 225, 190 + 135 * i),
            min_value, max_value, step
        )
        add_child(slider)
        sliders[slider_name] = slider

    reset_sliders()


func generate_rects():
    $UserInterface/Elapsed/ElapsedLabel.total_time = 0
    colors = []
    rects = []
    update()

    for i in range(sliders['rects_to_draw'].value):
        # rect position
        var vector_1 = Vector2(
            rng.randi_range(HALF_WIDTH, HALF_WIDTH),
            rng.randi_range(HALF_HEIGHT, HALF_HEIGHT)
        )
        # rect size
        var vector_2 = Vector2(
            rng.randi_range(sliders['rect_min_horiz'].value, sliders['rect_max_horiz'].value),
            rng.randi_range(sliders['rect_min_vert'].value, sliders['rect_max_vert'].value)
        )
        rects.append(Rect2(vector_1,vector_2))
        colors.append(base_colors[rng.randi_range(0, base_colors.size() - 1)])


func update_lever_values():
    for slider_name in sliders:
        var slider_value = sliders[slider_name].value

        # handle min/max overlaps
        if slider_name == 'rect_min_horiz':
            slider_value = min(slider_value, sliders['rect_max_horiz'].value)
        elif slider_name == 'rect_max_horiz':
            slider_value = max(slider_value, sliders['rect_min_horiz'].value + 1)
        elif slider_name == 'rect_min_vert':
            slider_value = min(slider_value, sliders['rect_max_vert'].value)
        elif slider_name == 'rect_max_vert':
            slider_value = max(slider_value, sliders['rect_min_vert'].value + 1)

        sliders[slider_name].value = slider_value
        # update labels
        count_labels[slider_name].text = str(slider_value)


func _reset():
    reset_labels()
    reset_sliders()


func reset_labels():
    count_labels['rects_to_draw'].text = str(rects_to_draw)
    count_labels['rect_min_horiz'].text = str(rect_min_horiz)
    count_labels['rect_max_horiz'].text = str(rect_max_horiz)
    count_labels['rect_min_vert'].text = str(rect_min_vert)
    count_labels['rect_max_vert'].text = str(rect_max_vert)
    count_labels['move_mult'].text = str(move_mult)


func reset_sliders():
    for slider_name in sliders:
        sliders[slider_name].value = get(slider_name)
