extends HSlider

func initialize(_name: String, size: Vector2, position: Vector2, _min_value: float, _max_value: float, _step: float):
    name = _name
    size_flags_vertical = 0
    set_size(size)
    set_position(position)
    min_value = _min_value
    max_value = _max_value
    step = _step
